export default {
    pageTitle: 'Marshall',
    welcomeHeading: 'Cofetărie și Cafenea',
    openings: 'Ore de deschidere',
    monFri: 'Lun - Vin',
    saturday: 'Sâmbătă',
    sunday: 'Duminică',
    contact: 'Contacte',
    about: 'Despre noi',
    aboutText: 'Ceva lung si simplu in romana despre pagina astea si despre shopul.'
}