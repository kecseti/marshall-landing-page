export default {
    pageTitle: 'Marshall',
    welcomeHeading: 'Cukrászda és kávéház',
    openings: 'Nyitvatartás',
    monFri: 'Hét - Pén',
    saturday: 'Szombat',
    sunday: 'Vasárnap',
    contact: 'Elérhetőségek',
    about: 'Rólunk',
    aboutText: 'Valami szép és hosszú magyar leírás mindarról ami ez az egész kegyere.'
}