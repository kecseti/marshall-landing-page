export default {
    pageTitle: 'Marshall',
    welcomeHeading: 'Pastry shop and coffee house',
    openings: 'Opening hours',
    monFri: 'Mon - Fri',
    saturday: 'Satruday',
    sunday: 'Sunday',
    contact: 'Contacts',
    about: 'About us',
    aboutText: 'Something long and beatifull about this page and the shop in english.'
}