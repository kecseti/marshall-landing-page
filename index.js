import huLang from './languages/hu.lang.js'
import enLang from './languages/eng.lang.js'
import roLang from './languages/ro.lang.js'

let openingNav;
let contactNav;
let aboutNav;
let openingNavMobile;
let contactNavMobile;
let aboutNavMobile;
let heading; 
let openings;
let monFri;
let saturday;
let sunday;
let contacts; 
let about;
let aboutText;
let openingFooter;
let contactFooter;
let aboutFooter;
let huButton;
let engButton;
let roButton;
let barContainer;
let dropdown;
let huButtonMobile;
let engButtonMobile;
let roButtonMobile;
let mobileNav;

function languageSet(lang){
    let displayLang = {};
    switch (lang){
        case 'eng':
            displayLang = enLang;
        break;

        case 'hu':
            displayLang = huLang;
        break;

        case 'ro':
            displayLang = roLang;
        break;
    }

    openingNav.textContent = displayLang.openings;
    contactNav.textContent = displayLang.contact;
    aboutNav.textContent = displayLang.about;
    openingNavMobile.textContent = displayLang.openings;
    contactNavMobile.textContent = displayLang.contact;
    aboutNavMobile.textContent = displayLang.about;
    heading.textContent = displayLang.welcomeHeading;
    openings.textContent = displayLang.openings;
    monFri.textContent = displayLang.monFri;
    saturday.textContent = displayLang.saturday;
    sunday.textContent = displayLang.sunday;
    contacts.textContent = displayLang.contact;
    about.textContent = displayLang.about;
    aboutText.textContent = displayLang.aboutText;
    openingFooter.textContent = displayLang.openings;
    contactFooter.textContent = displayLang.contact;
    aboutFooter.textContent = displayLang.about;

    console.log(displayLang)
}

function switchLanguage(lang){
    localStorage.setItem('lang',lang)
    languageSet(lang)
}

function setup(){
    openingNavMobile = document.getElementById('openingNavMobile');
    contactNavMobile = document.getElementById('contactNavMobile');
    aboutNavMobile = document.getElementById('aboutNavMobile');
    openingNav = document.getElementById('openingNav');
    contactNav = document.getElementById('contactNav');
    aboutNav = document.getElementById('aboutNav');
    heading = document.getElementById('heading');
    openings = document.getElementById('openings');
    monFri = document.getElementById('monFri');
    saturday = document.getElementById('saturday');
    sunday = document.getElementById('sunday');
    contacts= document.getElementById('contacts');
    about = document.getElementById('about');
    aboutText = document.getElementById('aboutText');
    openingFooter = document.getElementById('openingFooter')
    contactFooter = document.getElementById('contactFooter')
    aboutFooter = document.getElementById('aboutFooter')
    huButton = document.getElementById('huButton')
    engButton = document.getElementById('engButton')
    roButton = document.getElementById('roButton')
    huButtonMobile = document.getElementById('huButtonMobile')
    engButtonMobile = document.getElementById('engButtonMobile')
    roButtonMobile = document.getElementById('roButtonMobile')
    barContainer = document.getElementById('barContainer')
    dropdown = document.getElementById('dropdown')
    mobileNav = document.getElementById('mobileNav')

    const lang = localStorage.getItem('lang') || 'eng';
    languageSet(lang)

    huButton.addEventListener('click', () => switchLanguage('hu'))
    engButton.addEventListener('click', () => switchLanguage('eng'))
    roButton.addEventListener('click', () => switchLanguage('ro'))
    huButtonMobile.addEventListener('click', () => switchLanguage('hu'))
    engButtonMobile.addEventListener('click', () => switchLanguage('eng'))
    roButtonMobile.addEventListener('click', () => switchLanguage('ro'))

    barContainer.addEventListener('click', () => {
        dropdown.classList.toggle("open");
        mobileNav.classList.toggle("dropdown");
        barContainer.classList.toggle("change")
    })

}

var mymap = L.map('mapid').setView([46.672329, 25.502850], 13);
 L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoia2Vjc2kiLCJhIjoiY2traWN3MXlxMDhrbzJ1cXMzYmY3MzI4dSJ9.YWzNQWUgzgqZ7kSKVqxixw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoia2Vjc2kiLCJhIjoiY2traWN3MXlxMDhrbzJ1cXMzYmY3MzI4dSJ9.YWzNQWUgzgqZ7kSKVqxixw'
}).addTo(mymap);
var marker = L.marker([46.672329, 25.502850]).addTo(mymap);

setup();